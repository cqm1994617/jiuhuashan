export default {
//    "pages/tourRoute/index",
  data: {
    userInfo: {},
    isFullScreen: false,
    hasAuthLocation: true,
    hasAuthRecord: true,
    backgroundAudioId: null,
    backgroundAudioPause: false,
    tourRoute: {
      routeList: []
    },
    index: {
      innerAudioContextArr: [],
      showAuthModal: false,
      hasUserInfo: false
    },
    mapPage: {
      a: {}
    },
    detailPage: {
      richTextObj: {}
    },
    scenicIntroDetailPage: {
      scenicData: {},
      innerAudioContextArr: []
    },
    restaurantIntroDetailPage: {
      restData: {}
    },
    hotelIntroDetailPage: {
      hotelData: {}
    },
    airlineDetailPage: {
      airlineData: {}
    },
    nearLocPage: {
      nearLocData: {}
    },
    tourRoutePage: {
      tourline: {},
      tourlineStorage: []
    }
  }

}

import create from '../../utils/create'
import store from '../../store/index'
import requestObj from '../../utils/request'
import util from '../../utils/util'

const request = requestObj.request

create(store, {
  data: {
    isFullScreen: false,
    restaurantData: {},
    barActive: null,
    windowHeight: 0,
    pageHeight: 0,
    infoCardData: null,
    menuData: null,
    nearData: null,
    remarkData: null
  },
  onLoad(e) {
    request({
      url: `${util.host}/aide/saas/data/rest/detail?id=${e.id}`,
      method: 'GET',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then((res) => {
      let recoDish = []
      try {
        recoDish = JSON.parse(res.data.data.recoDish)
      } catch(e) {
        recoDish = []
      }
      this.setData({
        infoCardData: res.data.data,
        menuData: (recoDish && recoDish.length) > 0 ? recoDish : null
      })

      return request({
        url: `${util.host}/aide/saas/data/rest/reco?id=${res.data.data.id}&lng=${res.data.data.longitude}&lat=${res.data.data.latitude}`,
        method: 'GET',
        header: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
    }).then((res) => {
      this.setData({
        nearData: res.data.data
      })

      if (wx.nextTick) {
        wx.nextTick(() => {
          this.setInitHeight()
        })
      } else {
        setTimeout(() => {
          this.setInitHeight()
        }, 100)
      }
    })
  },
  setInitHeight() {
    const systemInfo = wx.getSystemInfoSync()
    const query = wx.createSelectorQuery()
    query.select('#page').boundingClientRect()
    query.exec((res) => {
      this.setData({
        pageHeight: res[0].height,
        windowHeight: systemInfo.windowHeight
      })
    })
  },
  onPageScroll(e) {
    const offset = this.data.pageHeight - this.data.windowHeight - 150
    if (e.scrollTop > 240 && e.scrollTop < 460) {
      this.setData({
        barActive: '1'
      })
    } else if (e.scrollTop > 460 && e.scrollTop < offset) {
      this.setData({
        barActive: '2'
      })
    } else if (e.scrollTop > offset) {
      this.setData({
        barActive: '3'
      })
    } else {
      this.setData({
        barActive: null
      })
    }
  },
  barSelect(e) {
    const id = e.currentTarget.dataset.barid
    if (id === '1') {
      wx.pageScrollTo({
        scrollTop: 250
      })
    }
    if (id === '2') {
      wx.pageScrollTo({
        scrollTop: 470
      })
    }
    if (id === '3') {
      wx.pageScrollTo({
        scrollTop: 9999
      })
    }
  }
})

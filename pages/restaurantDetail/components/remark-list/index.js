Component({
  properties: {
    remarkData: Object
  },
  data: {
    remarkTypeActive: '1'
  },
  methods: {
    selectRemark(e) {
      const id = e.currentTarget.dataset.typeid
      this.setData({
        remarkTypeActive: id
      })
    }
  }
})
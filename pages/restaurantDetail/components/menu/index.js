Component({
  properties: {
    menuData: Object
  },
  ready() {
    this.setData({
      menuList: this.data.menuData.map((item, index) => {
        item.dishImage = item.dishImage.indexOf('http') > -1 ? item.dishImage : "http:" + item.dishImage
        item.id = 'dish' + index
        return item
      })
    })
  }
})
Component({
  properties: {
    nearData: Object
  },
  data: {
    current: 0
  },
  ready() {
    this.setData({
      nearList: this.data.nearData.slice(0, 4).map(item => {
        item.score = (item.score / 10).toFixed(1)
        item.distance = item.distance > 1000 ? (item.distance / 1000).toFixed(1) + '千米' : item.distance + '米'
        return item
      })
    })
  },
  methods: {
    select(e) {
      const id = e.currentTarget.dataset.barid
      this.setData({
        current: id
      })
    },
    swiperChange(e) {
      const current = e.detail.current
      this.setData({
        current: current
      })
    },
    toRestaurant(e) {
      const id = e.currentTarget.dataset.restid
      wx.navigateTo({
        url: '../../../index?id=' + id
      })
    }
  }
})

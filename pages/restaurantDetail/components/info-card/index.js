Component({
  properties: {
    infoCardData: Object
  },
  data: {
    allComment: ''
  },
  ready() {
    const positiveComment = this.data.infoCardData.positiveComment || 0
    const negativeComment = this.data.infoCardData.negativeComment || 0

    this.setData({
      allComment: Number(positiveComment) + Number(negativeComment),
      score: (this.data.infoCardData.score / 10).toFixed(1)
    })
  },
  methods: {
    openLocation() {
      wx.openLocation({
        name: this.data.infoCardData.restaurantName,
        latitude: Number(this.data.infoCardData.latitude),
        longitude: Number(this.data.infoCardData.longitude),
        address: this.data.infoCardData.address
      })
    },
    phone() {
      console.log(this.data.infoCardData)
      if (this.data.infoCardData.contactNumber) {
        wx.makePhoneCall({
          phoneNumber: this.data.infoCardData.contactNumber.split(';')[0]
        })
      } else {
        wx.showToast({
          title: '暂无电话',
          icon: 'none'
        })
      }
    }
  }
})

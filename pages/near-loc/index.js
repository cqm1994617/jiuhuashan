const app = getApp()
import create from '../../utils/create'
import store from '../../store/index'

create(store, {
  data: {
    longitude: '',
    latitude: '',
    markers: [],
    showModal: false,
    selectMarker: {},
    modalOpacity: 0,
    containerBottom: 0
  },
  onLoad() {
    const markers = this.store.data.nearLocPage.nearLocData.map((item, index) => {
      const marker = {
        id: index,
        latitude: item.location.lat,
        longitude: item.location.lng,
        title: item.title,
        callout: {
          content: item.title,
          display: 'ALWAYS',
          padding: 5
        },
        address: item.address
      }
      return marker
    })
    wx.getLocation({
      type: 'gcj02',
      success: (res) => {
        this.setData({
          latitude: res.latitude,
          longitude: res.longitude,
          markers: markers
        })
      }
    })
  },
  closeModal() {
    this.setData({
      modalOpacity: 0,
      containerBottom: 0
    })
    setTimeout(() => {
      this.setData({
        showModal: false
      })
    }, 300)
  },
  onTap(e) {
    // const selectMarker = this.data.markers.filter(item => item.id === e.markerId)[0]
    // this.setData({
    //   showModal: true,
    //   selectMarker: selectMarker
    // })
    // console.log(selectMarker)
    // //模拟nextTick
    // setTimeout(() => {
    //   this.setData({
    //     modalOpacity: 1,
    //     containerBottom: -120
    //   })
    // }, 0)
    const selectMarker = this.data.markers.filter(item => item.id === e.markerId)[0]
    console.log(selectMarker)
    wx.openLocation({
      latitude: Number(selectMarker.latitude),
      longitude: Number(selectMarker.longitude),
      scale: 15,
      name: selectMarker.title,
      address: selectMarker.address
    })
  },
  markertap(e) {
    this.onTap(e)
  },
  callouttap(e) {
    this.onTap(e)
  }
})

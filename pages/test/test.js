import util from '../../utils/util'

const wxRequest = util.promisify(wx.request)

Page({
  data: {
    audioPlay: false,
    playSeek: 0,
    autoSeek: true,
    latitude: '',
    longitude: '',
    innerHTML: '',
    b: ''
  },
  onReady(option) {
    this.initAudio()
    this.initMap()

    this.record()

  },

  record() {
    const recorderManager = wx.getRecorderManager()

    recorderManager.onStop((res) => {
      wx.showToast({
        title: "1234"
      })
      wx.uploadFile({
        url: 'https://cqmfe.club/server-test',
        filePath: res.tempFilePath,
        formData: {
          user: 'test',
          aa: 'asdf'
        },
        name: 'file',
        success(res) {
          console.log(res)
        }
      })
    })

    recorderManager.start({
      duration: 2000,
      sampleRate: 44100,
      numberOfChannels: 1,
      encodeBitRate: 192000,
      format: 'aac',
      frameSize: 50
    })

    recorderManager.onError((res) => {
      wx.showToast({
        title: '失败'
      })
    })
    
  },
  initAudio() {
    this.audioCtx = wx.createInnerAudioContext()
    // this.audioCtx.src = 'http://47.110.151.199/preview/test.mp3'
    this.audioCtx.src = 'http://daoyou.worldmaipu.com/upload_resource/hotspot/audio/2018/0504/20180504234851F79G3DEXZF89J2GX4EOJ.mp3'
    this.audioCtx.onPlay(() => {
      this.audioCtx.onTimeUpdate((args) => {

        if (this.data.autoSeek) {
          this.setData({
            playSeek: Math.floor(this.audioCtx.currentTime)
          })
        }
      })
    })
  },
  initMap() {
    wx.getLocation({
      type: 'wgs84',
      success: (res) => {
        this.setData({
          latitude: res.latitude,
          longitude: res.longitude
        })
      }
    })
  },
  playAudio() {
    if (!this.data.audioPlay) {
      this.setData({
        audioPlay: true
      })
      this.audioCtx.play()
    }
  },
  qrcodeTest() {
    wx.scanCode({
      success(res) {
        console.log(res)
      }
    })
  },
  pauseAudio() {
    if (this.data.audioPlay) {
      this.setData({
        audioPlay: false
      })
      this.audioCtx.pause()
    }
  },
  seek(data) {
    this.audioCtx.seek(data.detail.value)
    this.setData({
      playSeek: data.detail.value,
      autoSeek: true
    })
  },
  scroll() {
    this.setData({
      autoSeek: false
    })
  },
  loadHTML() {
    const htmlStr = `<div>
      <p style="color: red">一段文字</p>
      <ul>
        <li>1</li>
        <li>2</li>
      </ul>
      <img src="http://47.110.151.199/preview/test.png" style="width: 250px" />
      <p>adsfjp<span style="font-weight: bold">加粗部分</span>aosdijf</p>
    </div>`
    this.setData({
      innerHTML: htmlStr
    })
  },
  toWebview() {
    wx.navigateTo({
      url: '../webview/index'
    })
  }
})

Page({
  data: {
    url: ''
  },
  onLoad(option) {
    const redirectUrl = option.redirectUrl
    const url = decodeURIComponent(redirectUrl)
    this.setData({
      url
    })
  }
})

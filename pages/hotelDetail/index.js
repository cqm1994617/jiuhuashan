import create from '../../utils/create'
import store from '../../store/index'
import requestObj from '../../utils/request'
import util from '../../utils/util'

const request = requestObj.request
let hotelName = ''
let hotelId = ''

create(store, {
  data: {
    isFullScreen: false,
    hotelData: {}
  },

  onShareAppMessage() {
    return {
      title: hotelName ? hotelName : '酒店介绍',
      path: '/pages/hotelDetail/index?id=' + hotelId
    }
  },

  onLoad(option) {
    if (option.id) {
      wx.showLoading({
        title: '加载中...'
      })

      request({
        url: `${util.host}/aide/saas/data/hotel/detail?id=${option.id}`
      }).then(res => {
        wx.hideLoading()
        const result = res.data

        hotelName = result.data.hotelName
        hotelId = result.data.id

        result.data.contactNum = result.data.contactNum ? result.data.contactNum.replace(/电话/g, '') : ''

        this.setData({
          hotelData: result.data
        })
      }).catch((e) => {
        console.log(e)
        wx.hideLoading()
      })
    } else {
      this.store.data.hotelIntroDetailPage.hotelData

      this.setData({
        hotelData: this.store.data.hotelIntroDetailPage.hotelData || {}
      })
    }
  },
  openMap() {
    wx.openLocation({
      latitude: this.data.hotelData.latitude - 0,
      longitude: this.data.hotelData.longitude - 0,
      name: this.data.hotelData.hotelName,
      address: this.data.hotelData.address
    })
  },
  phoneCall() {
    wx.makePhoneCall({
      phoneNumber: this.data.hotelData.contactNum
    })
  },
  showImageDetail(e) {
    const imgUrl = e.currentTarget.dataset.imgurl.indexOf('http') > -1 ? e.currentTarget.dataset.imgurl : 'http:' + e.currentTarget.dataset.imgurl

    wx.previewImage({
      urls: [imgUrl],
      current: imgUrl
    })
  },
  toOrder() {
    wx.navigateTo({
      url: `../webview/index?redirectUrl=${encodeURIComponent('https://cqmfe.club/preview/test?type=hotel&id=' + this.data.hotelData.id)}`
    })
  }

})

const app = getApp()
import create from '../../utils/create'
import store from '../../store/index'
import requestUtil from '../../utils/request'
import util from '../../utils/util'

const request = requestUtil.request

create(store, {

  data: {
    restList: [],
    nextPage: 1,
    total: '',
    sortNum: '0',
    isSortModalShow: false,
    scrollTop: 0,
    backgroundAudioId: null,
    backgroundAudioPause: false
  },

  onLoad() {
    this.backgroundAudio = wx.getBackgroundAudioManager()
    this.backgroundAudio.onStop(() => {
      this.store.data.backgroundAudioPause = false
      this.store.data.backgroundAudioId = null
      this.update()
    })
    this.backgroundAudio.onEnded(() => {
      this.store.data.backgroundAudioPause = false
      this.store.data.backgroundAudioId = null
      this.update()
    })
    
    request({
      url: `${util.host}/aide/saas/data/scenic/sort`,
      data: {
        pageSize: 20,
        pageNum: 1,
        sortNum: 0
      },
      withLocation: true
    }).then(res => {
      this.setData({
        restList: this.setDistanceAndScore(res.data.data.list),
        nextPage: 2,
        total: res.data.data.pages
      })
    })
  },

  more() {
    if (this.data.total >= this.data.nextPage) {
      const list = this.data.restList.slice()
      request({
        url: `${util.host}/aide/saas/data/scenic/sort`,
        data: {
          pageSize: 20,
          pageNum: this.data.nextPage,
          sortNum: this.data.sortNum
        },
        withLocation: true
      }).then(res => {
        this.setData({
          restList: this.setDistanceAndScore(list.concat(res.data.data.list)),
          nextPage: this.data.nextPage + 1
        })
      })
    }
  },

  setDistanceAndScore(arr) {
    arr.forEach(item => {
      item.distanceStr = item.distance > 1000 ? ((item.distance / 1000).toFixed(1) + '千米') : (item.distance + '米')
      item.rankNum = Number(item.rank)
    })
    return arr
  },

  openSortModal() {
    this.setData({
      isSortModalShow: true
    })

  },
  closeSortModal() {
    this.setData({
      isSortModalShow: false
    })
  },
  toggleSortModal() {
    if (this.data.isSortModalShow) {
      this.closeSortModal()
    } else {
      this.openSortModal()
    }
  },
  sortBy(e) {
    const sortNum = e.currentTarget.dataset.sortnum
    if (sortNum !== this.data.sortNum) {

      request({
        url: `${util.host}/aide/saas/data/scenic/sort`,
        data: {
          pageSize: 20,
          pageNum: 1,
          sortNum: sortNum
        },
        withLocation: true
      }).then(res => {
        this.setData({
          restList: this.setDistanceAndScore(res.data.data.list),
          nextPage: 2,
          total: res.data.data.pages,
          sortNum: sortNum,
          scrollTop: 0
        })
      })
    }
    this.closeSortModal()
  },
  toMap() {
    this.store.data.nearLocPage.nearLocData = this.data.restList.slice(0, 20).map(item => {
      return {
        id: item.id,
        location: {
          lat: item.lat,
          lng: item.lng
        },
        title: item.merchantName,
        address: item.address
      }
    })
    this.update()
    wx.navigateTo({
      url: '../near-loc/index'
    })
  },
  toDetail(e) {
    const id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '../scenicIntroDetail/index?id=' + id
    })
  },
  audioPlay(e) {

    if ((this.store.data.backgroundAudioId === e.currentTarget.dataset.voiceid) && (!this.store.data.backgroundAudioPause)) {
      //暂停
      this.store.data.backgroundAudioPause = !this.backgroundAudio.paused
      this.backgroundAudio.pause()
      this.update()
    } else {
      if (this.backgroundAudio.paused && (this.store.data.backgroundAudioId === e.currentTarget.dataset.voiceid)) {
        this.backgroundAudio.play()
        this.store.data.backgroundAudioPause = false
      } else {
        this.backgroundAudio.src = e.currentTarget.dataset.voiceurl
        this.backgroundAudio.title = e.currentTarget.dataset.title || ''
        this.store.data.backgroundAudioId = e.currentTarget.dataset.voiceid || ''
        this.store.data.backgroundAudioPause = false
        this.backgroundAudio.play()
      }
      this.update()
    }
  }

})

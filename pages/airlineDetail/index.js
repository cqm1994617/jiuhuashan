import create from '../../utils/create'
import store from '../../store/index'

create(store, {

  data: {
    airlineData: {}
  },
  
  onLoad() {
    this.setData({
      airlineData: this.store.data.airlineDetailPage.airlineData
    })
  },

  toOrder() {

    wx.navigateTo({
      url: `../webview/index?redirectUrl=${encodeURIComponent('https://cqmfe.club/preview/test?type=airline&id=' + this.data.airlineData.id)}`
    })
  }
})
import create from '../../utils/create'
import store from '../../store/index'
import util from '../../utils/util'

create(store, {
  data: {
    richText: '',
    richTextArr: [],
    richTextObj: {},
    title: ''
  },
  onLoad() {
    const htmlText = this.store.data.detailPage.richTextObj.content

    const richText = util.htmlTextToRichText(htmlText)

    const richTextArr = util.richTextToArray(richText)
    
    this.setData({
      richTextArr,
      title: this.store.data.detailPage.richTextObj.title || ''
    })
  },

  phoneCall(e) {
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.phone
    })
  },

  showImageDetail(e) {
    const imgUrl = e.currentTarget.dataset.imageurl.indexOf('http') > -1 ? e.currentTarget.dataset.imageurl : 'http:' + e.currentTarget.dataset.imageurl

    wx.previewImage({
      urls: [imgUrl],
      current: imgUrl
    })
  },
})

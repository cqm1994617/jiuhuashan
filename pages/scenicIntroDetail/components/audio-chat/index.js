import create from '../../../../utils/create'
import store from '../../../../store/index'

create({
  properties: {
    duration: Number,
    url: String,
    audioId: Number,
    audioName: String
  },

  data: {
    init: false,
    firstPlay: true,
    start: false,
    currentTime: 0,
    isScrolling: false,
    durationFormat: '',
    currentTimeFormat: '00:00',
    backgroundAudioId: null,
    backgroundAudioPause: null
  },

  ready() {
    this.backgroundAudioContext = wx.getBackgroundAudioManager()

    this.setData({
      durationFormat: this.timeFormat(this.data.duration)
    })
    if (this.data.audioId === this.store.data.backgroundAudioId) {
      this.initBackgroundAudio()
      const manager = wx.getBackgroundAudioManager()
      this.setData({
        start: !manager.paused,
        currentTime: Math.floor(manager.currentTime),
        currentTimeFormat: this.timeFormat(Math.floor(manager.currentTime))
      })
    }
  },

  methods: {
    initBackgroundAudio() {

      this.backgroundAudioContext.title = this.data.audioName

      if (this.data.audioId !== this.store.data.backgroundAudioId) {
        this.backgroundAudioContext.src = this.data.url

        this.store.data.backgroundAudioId = this.data.audioId
        this.update()
      }

      this.backgroundAudioContext.onPlay(() => {
        if (this.data.firstPlay) {
          wx.hideLoading()
          this.setData({
            firstPlay: false
          })
        }

        this.store.data.backgroundAudioPause = false
        this.update()
      })
      this.backgroundAudioContext.onPause(() => {

        this.store.data.backgroundAudioPause = true
        this.update()

        if (this.data.firstPlay) {
          wx.hideLoading()
        }

        this.setData({
          start: false
        })
      })

      this.backgroundAudioContext.onTimeUpdate(() => {
        if (!this.data.isScrolling) {
          this.setData({
            currentTime: Math.floor(this.backgroundAudioContext.currentTime),
            currentTimeFormat: this.timeFormat(Math.floor(this.backgroundAudioContext.currentTime))
          })
        }
      })

      this.backgroundAudioContext.onStop(() => {
        this.store.data.backgroundAudioPause = false
        this.store.data.backgroundAudioId = null
        this.update()
        this.setData({
          start: false,
          currentTime: 0,
          currentTimeFormat: '00:00'
        })
      })
      this.backgroundAudioContext.onEnded(() => {
        this.store.data.backgroundAudioPause = false
        this.store.data.backgroundAudioId = null
        this.update()
        this.setData({
          start: false,
          currentTime: 0,
          currentTimeFormat: '00:00'
        })
      })

    },
    audioToggle() {
      if (!this.data.init) {
        //初始化backgroundAudio
        this.initBackgroundAudio()
      }
      if (this.data.start) {
        this.backgroundAudioContext.pause()
      } else {
        if (this.data.firstPlay) {
          wx.showLoading({
            title: '音频加载中'
          })
        }

        if (this.backgroundAudioContext.src) {
          this.backgroundAudioContext.play()
        } else {
          this.backgroundAudioContext.src = this.data.url
        }
      }
      this.setData({
        start: !this.data.start,
        init: true
      })
    },
    scollingFinish(res) {
      this.backgroundAudioContext.seek(res.detail.value)
      this.setData({
        currentTime: res.detail.value,
        currentTimeFormat: this.timeFormat(res.detail.value),
        isScrolling: false
      })
    },
    scrolling() {
      if (!this.data.isScrolling) {
        this.setData({
          isScrolling: true
        })
      }
    },
    timeFormat(time) {
      return this.formatNumber(Math.floor(time / 60)) + ':' + this.formatNumber(time % 60)
    },
    formatNumber(num) {
      return num >= 10 ? num : '0' + num
    }
  }
})

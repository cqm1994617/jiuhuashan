import create from '../../utils/create'
import store from '../../store/index'
import requestObj from '../../utils/request'
import util from '../../utils/util'

const request = requestObj.request
let scenicId = ''
let scenicName = ''

create(store, {

  data: {
    id: null,
    isFullScreen: false,
    scenicIntroDetailPage: {},
    scenicData: {},
    audio: null,
    backgroundAudioId: null,
    backgroundAudioPause: null
  },

  onShareAppMessage() {
    return {
      title: scenicName ? scenicName : '景点介绍',
      path: '/pages/scenicIntroDetail/index?id=' + scenicId
    }
  },

  onLoad(option) {
    if (option.id) {
      scenicId = option.id

      wx.showLoading({
        title: '加载中...'
      })
      this.setData({
        id: option.id
      })
      request({
        url: `${util.host}/aide/saas/data/scenic/detail?id=${option.id}`
      }).then(res => {

        wx.hideLoading()
        const result = res.data

        scenicName = result.data.scenicName

        this.setData({
          scenicData: result.data
        })
        return request({
          url: `${util.host}/aide/saas/data/scenic/getVoiceById?scenicId=${result.data.id}`
        })
      }).then(res => {
        const result = res.data

        if (result.data) {
          this.setData({
            audio: result.data
          })
        }
      }).catch(err => {
        wx.hideLoading()
      })
    } else {
      this.setData({
        scenicData: this.store.data.scenicIntroDetailPage.scenicData || {}
      })
    }
  },

  openMap() {
    wx.openLocation({
      latitude: this.data.scenicData.latitude - 0,
      longitude: this.data.scenicData.longitude - 0,
      name: this.data.scenicData.scenicName,
      address: this.data.scenicData.address
    })
  },

  phoneCall() {
    wx.makePhoneCall({
      phoneNumber: this.data.scenicData.phone
    })
  },

  showImageDetail(e) {
    const imgUrl = e.currentTarget.dataset.imgurl.indexOf('http') > -1 ? e.currentTarget.dataset.imgurl : 'http:' + e.currentTarget.dataset.imgurl
    wx.previewImage({
      urls: [imgUrl],
      current: imgUrl
    })
  }

  // toOrder() {
  //   wx.navigateTo({
  //     url: `../webview/index?redirectUrl=${encodeURIComponent('https://cqmfe.club/preview/test?type=scenic&id=' + this.data.scenicData.id)}&type=scenic`
  //   })
  // }
})

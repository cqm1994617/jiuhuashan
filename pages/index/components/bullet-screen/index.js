let id = 0

Component({

  data: {
    list: []
  },

  ready() {
    setInterval(() => {
      this.add()
    }, 2000)
  },

  methods: {
    add() {
      const newList = this.data.list.concat([{
        id: id++,
        text: `用户A: 普陀山介绍`,
        line: 1 + Math.round(Math.random() * 5)
      }])

      this.setData({
        list: newList
      })
    },
    finish(finishId) {
      const newList = this.data.list.filter(item => item.id !== finishId)
      
      this.setData({
        list: newList
      })
    },
    animationEnd(e) {
      console.log(e.currentTarget.dataset.id)
      this.finish(e.currentTarget.dataset.id)
    }
  }
})

const app = getApp()
import create from '../../../../utils/create'
import util from '../../../../utils/util'

create({
  properties: {
    urlString: String
  },
  data: {
    url: '',
    text: ''
  },
  ready() {
    const str = this.data.urlString
    const url = str.slice(str.indexOf('(') + 1, str.indexOf(')'))
    const text = str.slice(str.indexOf('[') + 1, str.indexOf(']'))
    this.setData({
      url: url,
      text: text
    })
  },
  methods: {
    webUrlClick(e) {
      const url = e.currentTarget.dataset.url
      wx.navigateTo({
        url: '../../../../webview/index?redirectUrl=' + encodeURIComponent(url)
      })
    }
  }
})

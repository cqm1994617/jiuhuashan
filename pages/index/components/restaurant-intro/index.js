import create from '../../../../utils/create'

create({
  properties: {
    restaurantIntro: Object
  },

  data: {
    restData: {}
  },

  ready() {
    const restData = JSON.parse(this.data.restaurantIntro.content)

    restData.score = Number((restData.score / 10).toFixed(1))
    restData.avgPrice = restData.avgPrice.replace(/[^\d]/g, '')
    this.setData({
      restData: restData
    })
  },
  
  methods: {
    toRestaurantIntroDetail() {
      this.store.data.restaurantIntroDetailPage.restData = this.data.restData
      this.update()

      wx.navigateTo({
        url: '../../pages/restaurantDetail/index?id=' + Number(this.data.restData.id)
      })
    }
  }
})

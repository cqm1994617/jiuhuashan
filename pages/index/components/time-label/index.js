import util from '../../../../utils/util'

Component({
  properties: {
    timestamp: Number
  },
  data: {
    timeLabel: ''
  },
  ready() {
    if (this.data.timestamp) {
      const date = new Date(this.data.timestamp)
      let hour = date.getHours()
      let minutes = date.getMinutes()

      let morningOrAfternoon = ''


      if (hour > 12) {
        morningOrAfternoon = '下午'
        hour -= 12
      } else if (hour === 0) {
        morningOrAfternoon = ''
      } else {
        morningOrAfternoon = '上午'
      }

      this.setData({
        timeLabel: morningOrAfternoon + hour + ':' + util.digitalCompletion(minutes)
      })

    }
  }
})

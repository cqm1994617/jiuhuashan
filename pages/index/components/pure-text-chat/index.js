import util from '../../../../utils/util'

Component({

  properties: {
    text: String
  },

  data: {
    textArr: [],
    hasData: true
  },

  ready() {
    if (this.data.text) {
      const textArr = util.richTextToArray(this.data.text)
      this.setData({
        hasData: true,
        textArr: textArr
      })
    } else {
      this.setData({
        hasData: false
      })
    }
  },
 
  methods: {
    phoneCall(e) {
      wx.makePhoneCall({
        phoneNumber: e.currentTarget.dataset.phone
      })
    }
  }

})

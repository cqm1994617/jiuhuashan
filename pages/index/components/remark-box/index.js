import create from '../../../../utils/create'
import requestUtil from '../../../../utils/request'
import util from '../../../../utils/util'

const wxRequest = requestUtil.request

Component({
  properties: {
    remarkData: Object
  },
  data: {
    show: false,
    selected: false,
    enabled: true,
    selectType: ''
  },
  ready() {
    this.setData({
      show: true
    })
  },
  methods: {
    autoResponse(sat) {
      this.triggerEvent('remarkResponse', {
        id: Date.now(),
        me: false,
        value: {
          knowledge: {},
          text: {
            content: sat === 0 ? '感谢您给我们一个大大的赞！' : '您的意见已经收到，我们会努力改进的~'
          },
          type: 'Text',
          hideRemark: true
        }
      })
      this.setData({
        selected: true,
        selectType: sat === 0 ? 'like' : 'dislike'
      })
    },

    request(sat) {
      wxRequest({
        url: `${util.host}/aide/saas/chat/feedback`,
        method: 'POST',
        header: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: {
          "question": this.data.remarkData.question,
          "standardQuestion": this.data.remarkData.value.knowledge && this.data.remarkData.value.knowledge.title ? this.data.remarkData.value.knowledge.title : '',
          "answer": JSON.stringify(this.data.remarkData.value),
          "type": this.data.remarkData.value.type,
          "isSatisfied": sat
        }
      }).then(() => {
        this.autoResponse(sat)
      })
    },

    like() {
      if (!this.data.selected) {
        this.setData({
          selected: true,
          selectType: 'like'
        })
        this.request(0)
      }
    },
    dislike() {
      if (!this.data.selected) {
        this.setData({
          selected: true,
          selectType: 'like'
        })
        this.request(1)
      }
    }
  }
})

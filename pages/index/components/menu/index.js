import eventTracking from '../../../../utils/eventTracking'

Component({
  methods: {
    submit(e) {
      //e.currentTarget.dataset
      eventTracking('click', 'index_menu_item', e.currentTarget.dataset.recommend)
      this.triggerEvent('recommendConfirm', e.currentTarget.dataset)
    },
    scenicMap() {
      eventTracking('click', 'index_menu_item', '景区地图')
      wx.navigateTo({
        url: '../scenicMap/index'
      })
    },
    findCarParking() {
      eventTracking('click', 'index_menu_item', '找停车场')
      wx.navigateTo({
        url: '../carParking/index'
      })
    }
  }
})

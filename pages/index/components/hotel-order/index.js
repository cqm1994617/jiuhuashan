Component({
  properties: {
    hotelOrder: Object
  },
  data: {
    text: '即将为您跳转...'
  },
  ready() {
    this.setText()
    this.toOrder()
  },
  methods: {
    toOrder() {
      const id = Math.round(this.data.hotelOrder.content)
      setTimeout(() => {
        wx.navigateTo({
          url: `../webview/index?redirectUrl=${encodeURIComponent('https://cqmfe.club/preview/test?type=hotel&id=' + id)}`
        })
      }, 1000)
    },
    setText() {
      setTimeout(() => {
        this.setData({
          text: '已为您跳转至预订页'
        })
      }, 1500)
    }
  }
})

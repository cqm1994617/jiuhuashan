import util from '../../../../utils/util'

Component({

  properties: {
    recommend: {
      type: Array,
      value: []
    },
    value: Object,
    type: Number
  },

  data: {
    textAndLinkArr: []
  },

  ready() {
    if (this.data.type === 2) {
      let res = this.data.value.text.content || this.data.value.knowledge.content

      res = util.htmlTextToRichText(res)
      res = res.replace(/\$cat\$post_notice\$cat\$/g, '')
      let textAndLinkArr = util.richTextToArray(res)

      this.setData({
        textAndLinkArr: textAndLinkArr
      })
    }
  },


  methods: {
    submit(e) {
      this.triggerEvent('recommendConfirm', e.currentTarget.dataset)
    },
    phoneCall(e) {
      wx.makePhoneCall({
        phoneNumber: e.currentTarget.dataset.phone
      })
    }
  }
})

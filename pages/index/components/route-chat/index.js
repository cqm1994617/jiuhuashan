import create from '../../../../utils/create'

create({
  properties: {
    routeInfo: Object
  },
  data: {
    text: ''
  },
  ready() {
    const data = this.data.routeInfo && this.data.routeInfo.content ? JSON.parse(this.data.routeInfo.content) : ''
    this.setData({
      text: data.detail || ''
    })
  }
})

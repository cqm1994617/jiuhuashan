const app = getApp()
import create from '../../../../utils/create'
import util from '../../../../utils/util'
import eventTracking from '../../../../utils/eventTracking'

create({
  properties: {
    navigateStr: String
  },
  data: {
    url: '',
    text: ''
  },
  ready() {
    const str = this.data.navigateStr
    console.log(str)
    const url = str.slice(str.indexOf('(') + 1, str.indexOf(')'))
    const text = str.slice(str.indexOf('[') + 1, str.indexOf(']'))
    this.setData({
      url: url,
      text: text
    })
  },
  methods: {
    navigateLink(e) {
      const url = e.currentTarget.dataset.url
      const text = e.currentTarget.dataset.text
      eventTracking('click', 'navigate_link', text)
      wx.navigateTo({
        url: url
      })
    }
  }
})

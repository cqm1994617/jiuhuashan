Component({
  properties: {
    hotelText: String
  },
  data: {
    hotelList: [],
    recommendShop: null
  },
  ready() {
    const hotelList = JSON.parse(this.data.hotelText)
    hotelList.forEach(item => {
      if (item.distance >= 1000) {
        item.distance = (item.distance / 1000).toFixed(1) + '千米'
      } else if (item.distance) {
        item.distance = item.distance + '米'
      }
      if (item.score) {
        item.score = Number(item.score)
      }
      if (item.imgUrl && item.imgUrl.indexOf('http') < 0) {
        item.imgUrl = 'http:' + item.imgUrl
      }
    })
    this.setData({
      hotelList: hotelList.slice(0, 5).filter(item => {
        if (hotelList[5]) {
          return item.id !== hotelList[5].id
        }
        return true
      }),
      recommendShop: hotelList.slice(5)[0]
    })
  },
  methods: {
    toHotelDetail(e) {
      const bookPage = e.currentTarget.dataset.bookpage
      wx.navigateToMiniProgram({
        appId: 'wxd804d46617a8e29e',
        path: bookPage
      })
    },
    showMore() {
      wx.navigateTo({
        url: '../../../../hotelList/index'
      })
    }
  }
})

import create from '../../../../utils/create'

create({
  properties: {
    url: String,
    duration: Number,
    myInputId: Number,
    inputList: Object,
    voiceText: {
      type: String,
      value: ''
    }
  },

  data: {
    start: false,
    showText: false
  },

  ready() {
    this.innerAudioContext = wx.createInnerAudioContext()
    this.innerAudioContext.src = this.data.url

    this.innerAudioContext.onPlay(() => {
      try {
        this.store.data.index.innerAudioContextArr && this.store.data.index.innerAudioContextArr.forEach(item => {
          if (item !== this.innerAudioContext) {
            item.pause()
          }
        })
      } catch (e) {
        console.log(e)
      }
    })

    this.innerAudioContext.onPause(() => {
      this.setData({
        start: false
      })
    })

    this.innerAudioContext.onStop(() => {
      this.setData({
        start: false
      })
    })
    this.innerAudioContext.onEnded(() => {
      this.setData({
        start: false
      })
    })

    this.store.data.index.innerAudioContextArr.push(this.innerAudioContext)
    this.update()
  },

  methods: {
    playMySound() {
      if (this.data.start) {
        this.innerAudioContext.stop()
      } else {
        this.innerAudioContext.play()
      }
      this.setData({
        start: !this.data.start
      })
    },

    transformText() {
      this.setData({
        showText: true
      })
    }
  }
})

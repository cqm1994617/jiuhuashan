Component({

  properties: {
    content: String
  },

  data: {
    status: 0,
    text: '即将跳转至投诉页...'
  },

  ready() {
    setTimeout(() => {
      wx.navigateTo({
        url: '../../../../complain/index?content=' + this.data.content
      })

      this.setData({
        status: 1,
        text: '点击跳转至投诉页'
      })
    }, 1000)
  },

  methods: {
    toComplain() {
      wx.navigateTo({
        url: '../../../../complain/index?content=' + this.data.content
      })
    }
  }
})

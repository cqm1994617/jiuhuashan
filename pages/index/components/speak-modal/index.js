let index = 1;

Component({
  data: {
    ellipsis: '.'
  },

  ready() {
    this.timer = setInterval(() => {
      let ellipsis = ''
      for (let i = 0; i < index; i++) {
        ellipsis += '.'
      }
      index = (index + 1) % 4

      this.setData({
        ellipsis
      })
    }, 500)
  },

  detached() {
    index = 1
    clearInterval(this.timer)
  }
})

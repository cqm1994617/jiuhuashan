import create from '../../../../utils/create'

create({
  properties: {
    hotelIntro: Object
  },

  data: {
    hotelData: {}
  },

  ready() {
    const hotelData = JSON.parse(this.data.hotelIntro.content)

    hotelData.contactNum = hotelData.contactNum ? hotelData.contactNum.replace(/电话/g, '') : ''

    hotelData.score = Number(hotelData.score)

    this.setData({
      hotelData: hotelData
    })
  },

  methods: {
    toHotelDetail(e) {
      this.store.data.hotelIntroDetailPage.hotelData = this.data.hotelData
      this.update()

      const bookPage = e.currentTarget.dataset.bookpage
      wx.navigateToMiniProgram({
        appId: 'wxd804d46617a8e29e',
        path: bookPage
      })
    }
  }
})

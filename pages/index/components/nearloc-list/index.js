import create from '../../../../utils/create'

create({
  properties: {
    nearlocData: Object
  },

  data: {
    status: 0,
    text: '即将跳转至地图页...'
  },

  ready() {

    const dataJSON = this.data.nearlocData.content
    let data = JSON.parse(dataJSON)

    this.store.data.nearLocPage.nearLocData = data
    this.update()

    setTimeout(() => {
      wx.navigateTo({
        url: '../../../../near-loc/index'
      })

      this.setData({
        status: 1,
        text: '点击跳转至地图页'
      })
    }, 1000)
  },

  methods: {
    toLocation() {
      const dataJSON = this.data.nearlocData.content
      let data = JSON.parse(dataJSON)

      this.store.data.nearLocPage.nearLocData = data
      this.update()
      wx.navigateTo({
        url: '../../../../near-loc/index'
      })
    }
  }


})

const app = getApp()
import create from '../../../../utils/create'
import util from '../../../../utils/util'
import eventTracking from '../../../../utils/eventTracking'

create({
  properties: {
    richText: String
  },
  data: {
    richArr: [],
    summary: [],
    hasMore: false
  },
  ready() {
    var flagHasMore = false
    const richText = util.htmlTextToRichText(this.data.richText)

    const richArr = util.richTextToArray(richText)

    const summaryObj = util.getSummary(richArr)

    if (typeof richText === 'string' && richText.indexOf('$link$') > -1) {
      flagHasMore = false
    } else {
      flagHasMore = true
    }

    this.setData({
      richArr,
      summary: flagHasMore ? summaryObj.summary : richArr,
      hasMore: flagHasMore ? summaryObj.hasMore : false
    })
  },
  methods: {
    showImageDetail(e) {
      const imgUrl = e.currentTarget.dataset.imageurl

      wx.previewImage({
        urls: [imgUrl],
        current: imgUrl
      })
    },
    toDetail() {
      this.store.data.detailPage.richTextObj.content = this.data.richText
      eventTracking('click', 'pages/index/index', {cnName:'阅读全文', enName:'enter_richtext'})
      wx.navigateTo({
        url: '../../../../richTextDetail/index'
      })
    },
    phoneCall(e) {
      eventTracking('click', 'pages/index/index', {cnName:'电话链接', enName:'remark_phone_link'})
      wx.makePhoneCall({
        phoneNumber: e.currentTarget.dataset.phone
      })
    },
    webUrlClick(e) {
      console.log(e)
    },
    submit(e) {
      eventTracking('click', 'pages/index/index', {cnName:e.currentTarget.dataset.recommend, enName:'richtext_link'})
      this.triggerEvent('recommendConfirm', e.currentTarget.dataset)
    }
  }
})

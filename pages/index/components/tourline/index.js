import create from '../../../../utils/create'

create({
  properties: {
    tourlineInfo: Object
  },
  data: {
    tourline: []
  },
  ready() {
    const tourline = JSON.parse(this.data.tourlineInfo.content)
    this.store.data.tourRoute.tourlineStorage = tourline.map(item => item.hashValue)
    this.setData({
      tourline: tourline
    })
  },
  methods: {
    tourRoute(e) {
      const data = e.currentTarget.dataset.tourline
      const id = e.currentTarget.dataset.id

      this.update()
      wx.navigateTo({
        url: '../../../../tourRoute/index?id=' + id
      })
    }
  }
})

import create from '../../../../utils/create'
import util from '../../../../utils/util'

create({

  properties: {
    navigateStr: String
  },

  data: {
    status: 0,
    text: '即将跳转...',
    url: ''
  },

  ready() {
    setTimeout(() => {

      const str = this.data.navigateStr
      const text = str.slice(str.indexOf('[') + 1, str.indexOf(']'))
      const url = str.slice(str.indexOf('(') + 1, str.indexOf(')'))

      this.setData({
        status: 1,
        text: text,
        url: url
      })

      wx.navigateTo({
        url: url
      })

    }, 500)
  },

  methods: {
    navigate() {
      wx.navigateTo({
        url: this.data.url
      })
    }
  }
})

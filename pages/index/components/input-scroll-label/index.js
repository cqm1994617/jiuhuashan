import util from '../../../../utils/util'
import wxRequest from '../../../../utils/request'
import eventTracking from '../../../../utils/eventTracking'

const request = wxRequest.request
let time = Date.now()
const backupQuestion = []
const minutesArr = ['半分钟前', '1分钟前', '2分钟前', '3分钟前', '4分钟前', '5分钟前']
let pollTimer = null
let firstShow = true

Component({
  data: {
    labelList: [],
    tipsPosition: 50,
    toolTipList: [],
    toolTipText: ''
  },

  ready() {
    this.getListFromCache()

    request({
      url: `${util.host}/aide/saas/navigation/list?platId=1`,
      method: 'get'
    }).then(res => {
      const toolTipList = res.data.data || []
      this.setData({
        toolTipList: toolTipList
      })
    })
  },

  methods: {
    recommendConfirm(e) {
      const dataset = e.currentTarget.dataset
      eventTracking('click', 'index_scroll_label', dataset.recommend)
      if (dataset.label.type === 'wxapp_link') {
        wx.navigateToMiniProgram({
          appId: dataset.label.data.split('|')[0],
          path: dataset.label.data.split('|')[1]
        })
      } else {
        if (Date.now() - time > 500) {
          time = Date.now()
          this.triggerEvent('recommendConfirm', dataset)
        }
      }
    },
    getListFromCache() {
      try {
        const labelStr = wx.getStorageSync('inputScrollLabel')
        const labelList = JSON.parse(labelStr)
        this.setData({
          labelList
        })
      } catch (e) {

      }
    },
    setCache(list) {
      try {
        wx.setStorageSync('inputScrollLabel', JSON.stringify(list))
      } catch (e) {}
    },
    dataCollect() {
      wx.navigateTo({
        url: '../../../../dataCollect/index'
      })
    }
  }
})

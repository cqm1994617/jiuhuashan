import create from '../../../../utils/create'
import store from '../../../../store/index'

const app = getApp()

create({
  data: {
    userInfo: {},
    questionIndex: 0,
    allQuestion: [
      { id: 0, question: '普陀山介绍' },
      { id: 1, question: '附近的酒店' },
      { id: 2, question: '普陀山有什么景点' },
      { id: 3, question: '附近的餐厅' },
      { id: 4, question: '普陀山历史' },
      { id: 5, question: '我要投诉' }
    ],
    questionList: []
  },
  ready() {
    this.changeQuestion()
  },
  methods: {
    changeQuestion() {
      this.setData({
        questionList: this.data.allQuestion.slice(this.data.questionIndex, this.data.questionIndex + 3),
        questionIndex: (this.data.questionIndex + 3) % this.data.allQuestion.length
      })
    },
    submit(e) {
      //e.currentTarget.dataset
      this.triggerEvent('recommendConfirm', e.currentTarget.dataset)
    },
    help() {
      wx.navigateTo({
        url: '../../../../help/index'
      })
    }
  }
})

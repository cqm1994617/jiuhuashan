Component({

  properties: {
    latitude: Number,
    longitude: Number
  },

  data: {
    status: 0,
    text: '即将跳转至地图页...'
  },

  ready() {
    setTimeout(() => {

      wx.openLocation({
        latitude: this.data.latitude,
        longitude: this.data.longitude,
        name: '那年传统菜(赛银国际广场店)',
        address: '高教路赛银国际广场1楼'
      })

      this.setData({
        status: 1,
        text: '点击跳转至地图页'
      })
    }, 1000)
  },

  methods: {
    toLocation() {

      wx.openLocation({
        latitude: this.data.latitude,
        longitude: this.data.longitude,
        name: '定位点A',
        address: `经纬度：${this.data.latitude}, ${this.data.longitude}`
      })
    }
  }
})

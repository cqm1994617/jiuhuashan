import create from '../../../../utils/create'

create({
  properties: {
    airlineData: Object
  },
  data: {
    airlineList: []
  },
  ready() {
    let airlineList = this.data.airlineData.content ? JSON.parse(this.data.airlineData.content) : ''
    airlineList.forEach(item => {
      item.accuracy = Math.round(item.accuracy * 100) + '%'
      if (item.schedual) {
        item.schedual = item.schedual.replace(/\'/g, '\"')
        item.schedual = JSON.parse(item.schedual).map(item => '周' + item).join('，')
      }
      if (item.arrivalTime) {
        item.arrivalTime = item.arrivalTime.split('.')[0] || ''
      }
      if (item.departureTime) {
        item.departureTime = item.departureTime.split('.')[0] || ''
      }
    })
    this.setData({
      airlineList: airlineList
    })
  },
  methods: {
    toAirlineDetail(e) {
      const id = e.currentTarget.dataset.id
      const data = this.data.airlineList.filter(item => item.id === id)[0]

      this.store.data.airlineDetailPage.airlineData = data
      this.update()
      wx.navigateTo({
        url: '../../pages/airlineDetail/index?id=' + e.currentTarget.dataset.id
      })
    }
  }
})

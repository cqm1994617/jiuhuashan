import create from '../../../../utils/create'

create({
  properties: {
    scenicText: String
  },

  data: {
    scenicList: [],
    recommendShop: null,
    backgroundAudioId: null,
    backgroundAudioPause: null
  },

  ready() {
    const scenicList = JSON.parse(this.data.scenicText)
    this.backgroundAudio = wx.getBackgroundAudioManager()
    this.backgroundAudio.onStop(() => {
      this.store.data.backgroundAudioPause = false
      this.store.data.backgroundAudioId = null
      this.update()
    })
    this.backgroundAudio.onEnded(() => {
      this.store.data.backgroundAudioPause = false
      this.store.data.backgroundAudioId = null
      this.update()
    })

    scenicList.forEach(item => {
      if (item.distance >= 1000) {
        item.distance = (item.distance / 1000).toFixed(1) + '千米'
      } else if (item.distance) {
        item.distance = item.distance + '米'
      }
      if (item.rank) {
        item.rank = Number(item.rank)
      }
      if (item.url && item.url.indexOf('http') < 0) {
        item.url = 'http:' + item.url
      }
    })
    this.setData({
      scenicList: scenicList.slice(0, 5).filter(item => {
        if (scenicList[5]) {
          return item.id !== scenicList[5].id
        }
        return true
      }),
      recommendShop: scenicList.slice(5)[0]
    })
  },

  methods: {
    toScenicDetail(e) {
      const id = e.currentTarget.dataset.id || ''
      wx.navigateTo({
        url: '../../../../scenicIntroDetail/index?id=' + id
      })
    },
    showMore() {
      wx.navigateTo({
        url: '../../../../scenicList/index'
      })
    },
    audioPlay(e) {
      console.log(e.currentTarget.dataset)
      if ((this.store.data.backgroundAudioId === e.currentTarget.dataset.voiceid) && (!this.store.data.backgroundAudioPause)) {
        //暂停
        this.store.data.backgroundAudioPause = !this.backgroundAudio.paused
        this.backgroundAudio.pause()
        this.update()
      } else {
        if (this.backgroundAudio.paused && (this.store.data.backgroundAudioId === e.currentTarget.dataset.voiceid)) {
          this.backgroundAudio.play()
          this.store.data.backgroundAudioPause = false
        } else {
          this.backgroundAudio.src = e.currentTarget.dataset.voiceurl
          this.backgroundAudio.title = e.currentTarget.dataset.title || ''
          this.store.data.backgroundAudioId = e.currentTarget.dataset.voiceid || ''
          this.store.data.backgroundAudioPause = false
          this.backgroundAudio.play()
        }
        this.update()
      }
    }
  }

})

import create from '../../../../utils/create'
import requestObj from '../../../../utils/request'
import util from '../../../../utils/util'

const request = requestObj.request

create({

  properties: {
    scenicIntro: Object
  },

  data: {
    scenicName: '',
    description: '',
    coverImagesUrl: '',
    rank: '',
    transportation: '',
    scenicData: {},
    backgroundAudioId: null,
    backgroundAudioPause: null
  },

  ready() {
    const data = JSON.parse(this.data.scenicIntro.content)
    this.backgroundAudio = wx.getBackgroundAudioManager()
    this.backgroundAudio.onStop(() => {
      this.store.data.backgroundAudioPause = false
      this.store.data.backgroundAudioId = null
      this.update()
    })
    this.backgroundAudio.onEnded(() => {
      this.store.data.backgroundAudioPause = false
      this.store.data.backgroundAudioId = null
      this.update()
    })

    if (data.rank) {
      data.rank = Number(data.rank)
    }

    request({
      url: `${util.host}/aide/saas/data/scenic/getVoiceById?scenicId=${data.id}`
    }).then(res => {

      if (res.data.data) {
        data.voiceUrl = res.data.data.url
        data.voiceId = res.data.data.id
      }

      this.setData({
        scenicData: data,
        scenicName: data.scenicName,
        description: data.description.length > 50 ? data.description.slice(0, 50) + '...' : data.description,
        address: data.address,
        rank: Number(data.rank),
        transportation: data.transportation || ''
      })
    })

  },
  methods: {
    toScenicIntroDetail() {
      this.store.data.scenicIntroDetailPage.scenicData = this.data.scenicData
      this.update()
      wx.navigateTo({
        url: '../../pages/scenicIntroDetail/index?id=' + this.data.scenicData.id
      })
    },
    audioPlay(e) {
      if ((this.store.data.backgroundAudioId === e.currentTarget.dataset.voiceid) && (!this.store.data.backgroundAudioPause)) {
        //暂停
        this.store.data.backgroundAudioPause = !this.backgroundAudio.paused
        this.backgroundAudio.pause()
        this.update()
      } else {
        if (this.backgroundAudio.paused && (this.store.data.backgroundAudioId === e.currentTarget.dataset.voiceid)) {
          this.backgroundAudio.play()
          this.store.data.backgroundAudioPause = false
        } else {
          this.backgroundAudio.src = e.currentTarget.dataset.voiceurl
          this.backgroundAudio.title = e.currentTarget.dataset.title || ''
          this.store.data.backgroundAudioId = e.currentTarget.dataset.voiceid || ''
          this.store.data.backgroundAudioPause = false
          this.backgroundAudio.play()
        }
        this.update()
      }
    }
  }
})

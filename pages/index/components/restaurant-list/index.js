Component({
  properties: {
    restaurantText: String
  },
  data: {
    restaurantList: [],
    recommendShop: null
  },
  ready() {
    const restaurantList = JSON.parse(this.data.restaurantText)
    restaurantList.forEach(item => {
      if (item.distance >= 1000) {
        item.distance = (item.distance / 1000).toFixed(1) + '千米'
      } else if (item.distance) {
        item.distance = item.distance + '米'
      }
      if (item.score) {
        item.score = item.score / 10
      }
      if (item.imgUrl && item.imgUrl.indexOf('http') < 0) {
        item.imgUrl = 'http:' + item.imgUrl
      }
    })

    this.setData({
      restaurantList: restaurantList.slice(0, 5),
      recommendShop: restaurantList.slice(5)[0]
    })
  },
  methods: {
    toRestaurantDetail(e) {
      const id = e.currentTarget.dataset.id
      console.log(e.currentTarget)
      wx.navigateTo({
        url: '../../../../restaurantDetail/index?id=' + id
      })
    },
    showMore() {
      wx.navigateTo({
        url: '../../../../restaurantList/index'
      })
    }
  }
})

import util from '../../../../utils/util'
import create from '../../../../utils/create'
import requestObj from '../../../../utils/request'

const request = requestObj.request

create({

  data: {
    richText: '',
    richArr: [],
    hasData: false
  },
  ready() {

    request({
      url: `${util.host}/aide/saas/data/inform/getImpoInfo?ques=最近重要通知`
    }).then(res => {
      
      const data = res.data.data ? res.data.data.replace(/\$cat\$post_notice\$cat\$/g, '') : ''
      
      const withoutSpace = data.replace(/\ +/g,"").replace(/[\r\n]/g,"")

      if (!withoutSpace) {
        return
      }

      const richText = util.htmlTextToRichText(data)

      const richArr = util.richTextToArray(richText)
  
      this.setData({
        richArr,
        hasData: true
      })
    })

  },
  methods: {
    showImageDetail(e) {
      const imgUrl = e.currentTarget.dataset.imageurl

      wx.previewImage({
        urls: [imgUrl],
        current: imgUrl
      })
    },
    phoneCall(e) {
      wx.makePhoneCall({
        phoneNumber: e.currentTarget.dataset.phone
      })
    }
  }
})
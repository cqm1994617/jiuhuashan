Component({
  data: {
    text: '即将为您跳转...'
  },
  ready() {
    this.setText()
    this.toOrder()
  },
  methods: {
    toOrder() {
      setTimeout(() => {
        wx.navigateTo({
          url: `../webview/index?redirectUrl=${encodeURIComponent('https://cqmfe.club/preview/test?type=ship')}`
        })
      }, 1000)
    },
    setText() {
      setTimeout(() => {
        this.setData({
          text: '已为您跳转至预订页'
        })
      }, 1500)
    }
  }
})

Component({
  properties: {
    weatherInfo: Object
  },

  data: {
    weather: {},
    futureList: []
  },

  ready() {
    const content = JSON.parse(this.data.weatherInfo.content)
    const weather = JSON.parse(content)
    console.log(weather)
    const futureList = weather.data.slice(1, 6).map((item, index) => ({
      listId: 'weather_' + index,
      wea: item.wea,
      wea_img: item.wea_img,
      date: index === 0 ? '明天' : (new Date(item.date)).getDate() + '日',
      tem: item.tem
    }))
    this.setData({
      weather: weather,
      futureList: futureList
    })
  }
})
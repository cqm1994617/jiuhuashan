import create from '../../../../utils/create'
import store from '../../../../store/index'

create({
  properties: {
    duration: Number,
    url: String
  },

  data: {
    firstPlay: true,
    start: false,
    currentTime: 0,
    isScrolling: false,
    durationFormat: '',
    currentTimeFormat: '00:00'
  },

  ready() {
    this.setData({
      durationFormat: this.timeFormat(this.data.duration)
    })

    this.innerAudioContext = wx.createInnerAudioContext()
    this.innerAudioContext.src = this.data.url
    this.innerAudioContext.onPlay(() => {
      if (this.data.firstPlay) {
        wx.hideLoading()
        this.setData({
          firstPlay: false
        })
      }

      this.store.data.index.innerAudioContextArr && this.store.data.index.innerAudioContextArr.forEach(item => {
        if (item !== this.innerAudioContext) {
          item.pause()
        }
      })

    })

    this.innerAudioContext.onPause(() => {

      if (this.data.firstPlay) {
        wx.hideLoading()
      }

      this.setData({
        start: false
      })
    })

    this.innerAudioContext.onTimeUpdate(() => {
      if (!this.data.isScrolling) {
        this.setData({
          currentTime: Math.floor(this.innerAudioContext.currentTime),
          currentTimeFormat: this.timeFormat(Math.floor(this.innerAudioContext.currentTime))
        })
      }
    })

    this.innerAudioContext.onStop(() => {
      this.setData({
        start: false
      })
    })
    this.innerAudioContext.onEnded(() => {
      this.setData({
        start: false,
        currentTime: 0,
        currentTimeFormat: '00:00'
      })
    })

    this.store.data.index.innerAudioContextArr.push(this.innerAudioContext)
    this.update()

  },

  methods: {
    audioToggle() {
      if (this.data.start) {
        this.innerAudioContext.pause()
      } else {
        if (this.data.firstPlay) {
          wx.showLoading({
            title: '音频加载中'
          })
        }

        this.innerAudioContext.play()
      }
      this.setData({
        start: !this.data.start
      })
    },
    scollingFinish(res) {
      this.innerAudioContext.seek(res.detail.value)
      this.setData({
        currentTime: res.detail.value,
        currentTimeFormat: this.timeFormat(res.detail.value),
        isScrolling: false
      })
    },
    scrolling() {
      if (!this.data.isScrolling) {
        this.setData({
          isScrolling: true
        })
      }
    },
    timeFormat(time) {
      return this.formatNumber(Math.floor(time / 60)) + ':' + this.formatNumber(time % 60)
    },
    formatNumber(num) {
      return num >= 10 ? num : '0' + num
    }
  }
})

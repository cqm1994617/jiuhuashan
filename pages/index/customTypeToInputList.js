function textFunc(result) {
  return {
    id: result.data.sessionId,
    value: {
      type: result.data.answerDataType,
      data: result.data.answer
    },
    me: false,
    sourceId: result.sourceId,
    question: result.question
  }
}

function jsonFunc(result) {
  try {
    var answer = JSON.parse(result.data.answer)
    console.log(answer)
    var answerData = JSON.stringify(answer.data)
  } catch (error) {
    console.log(error)
  }
  return {
    id: result.data.sessionId,
    value: {
      // hideRemark: true,
      type: result.data.type,
      // data: result.data.answer
      data: {
        content: answerData
      }
    },
    me: false,
    sourceId: result.sourceId,
    question: result.question
  }
}

// 旧版
function yunxmFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: item,
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function sceniclistFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function scenicintroFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function complainFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        hideRemark: true,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function hotellistFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function restaurantlistFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function hotelintroFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function restaurantintroFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function airlineFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function restaurantidFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        hideRemark: true,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function scenicidFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        hideRemark: true,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function hotelidFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        hideRemark: true,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function shiporderFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        hideRemark: true,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function nearlocFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function routeFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function weatherFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function tourlineFunc(result) {
  return result.data.messages.map(item => {
    return {
      id: result.data.sessionId,
      value: {
        type: result.type,
        data: item.text
      },
      me: false,
      sourceId: result.sourceId,
      question: result.question
    }
  })
}

function customTypeToInputList(result) {
  const typeObj = {
    text: textFunc,
    json: jsonFunc
  }

  if (typeObj[result.data.answerDataType]) {
    return typeObj[result.data.answerDataType](result)
  } else {
    return typeObj['text'](result)
    // throw new Error(`错误的type: ${result.type}`)
  }
}

export default customTypeToInputList

import create from '../../utils/create'
import store from '../../store/index'
import util from '../../utils/util'
import requestUtil from '../../utils/request'
import customTypeToInputList from './customTypeToInputList'
import eventTracking from '../../utils/eventTracking'

const request = requestUtil.request
const upload = requestUtil.upload

let speakDuration = 0
let sessionId = ''
let recording = false

let myInputId = 0

let temp = 0

create(store, {
  data: {
    showMenu: false,
    index: {},
    hasAuthLocation: true,
    hasAuthRecord: true,
    inputWord: '',
    inputList: [],
    mode: "text",
    isFullScreen: false,
    toView: 'toView',
    showSpeakModal: false,
    backgroundAudioId: null,
    backgroundAudioPause: null,
    shareImageUrl: ''
  },

  onLoad() {
    wx.showShareMenu({
      withShareTicket: true
    })
    this.initRecorderManager()
    this.initShareUrl()
  },

  onShareAppMessage() {
    return {
      title: '有事问小听',
      path: '/pages/index/index',
      imageUrl: this.data.shareImageUrl || 'http://aidehelper.oss-cn-hangzhou.aliyuncs.com/aide/2019-10-30/2ef6debd5f5b42ad95ffa6d8743516cf-%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20191030133256.jpg?Expires=4726013621&OSSAccessKeyId=LTAIgAYIGiRPPeMn&Signature=LL0zmW7fPzHNHpLfB6hX8QG0mII%3D'
    }
  },

  onHide() {
    this.store.data.index.innerAudioContextArr && this.store.data.index.innerAudioContextArr.forEach(item => {
      item.pause()
    })
  },

  initShareUrl() {
    request({
      url: `${util.host}/aide/saas/theme/get`,
      method: 'GET'
    }).then(res => {
      if (res.data.data) {
        this.setData({
          shareImageUrl: res.data.data.url
        })
      }
    })
  },

  initRecorderManager() {
    this.recorderManager = wx.getRecorderManager()

    this.recorderManager.onStart(() => {
      wx.showToast({
        title: '录音中...',
        image: '../../img/soundLogo.png',
        duration: 60000
      })
    })

    this.recorderManager.onError((err) => {
      console.log(err)
    })

    this.recorderManager.onStop((res) => {
      wx.hideToast()
      recording = false
      const inputList = this.data.inputList.concat()
      if (this.isTimeStampShow(inputList)) {
        inputList.push({
          id: Date.now() + 'timestamp',
          value: Date.now(),
          type: 'time'
        })
      }

      inputList.push({
        id: myInputId,
        me: true,
        value: res.tempFilePath,
        type: 'voice',
        duration: speakDuration > 100 ? 1 : speakDuration
      })
      this.setData({
        inputList
      })

      setTimeout(() => {
        this.scrollToBottom()
      }, 300)

      wx.showLoading({
        title: '语音识别中...'
      })

      this.fetchData({
        type: 'voice',
        myInputId: myInputId,
        tempFilePath: res.tempFilePath
      })

      myInputId++
    })

  },

  input(e) {
    this.setData({
      inputWord: e.detail.value
    })
  },
  send() {
    if (this.data.inputWord) {
      this.submit(this.data.inputWord)
    } else {
      wx.showToast({
        title: '问题不得为空',
        icon: 'none'
      })
    }
  },
  confirm() {
    this.submit(this.data.inputWord)
  },
  isTimeStampShow(inputList) {
    const timeArrayList = inputList.filter(item => item.type === 'time')
    if (timeArrayList.length > 0) {
      if (Date.now() - timeArrayList[timeArrayList.length - 1].value > 60000) {
        return true
      } else {
        return false
      }
    }
    return true
  },
  submit(value) {

    if (!value) {
      return
    }

    const inputList = this.data.inputList.concat()

    if (this.isTimeStampShow(inputList)) {
      inputList.push({
        id: Date.now() + 'timestamp',
        value: Date.now(),
        type: 'time'
      })
    }

    inputList.push({
      id: myInputId,
      me: true,
      value,
      type: 'text'
    })

    this.fetchData({
      myInputId: myInputId
    })

    myInputId++

    this.setData({
      inputList,
      inputWord: '',
      toView: ''
    })

    this.scrollToBottom()

  },
  recommendConfirm(e) {
    const recommend = e.detail.recommend || ''
    this.setData({
      inputWord: recommend || ''
    })

    setTimeout(() => {
      this.submit(recommend || '')
    }, 100)

  },

  remarkResponse(res) {
    const inputList = this.data.inputList.concat(res.detail)
    this.setData({
      inputList: inputList
    })
    setTimeout(() => {
      this.scrollToBottom()
    }, 300)
  },

  fetchErrFunc() {
    const inputList = this.data.inputList.concat([{
      id: myInputId++,
      value: {
        type: 'Text',
        content: '您搜索的问题暂时没有答案。'
      },
      me: false
    }])
    this.setData({
      inputList
    })
    setTimeout(() => {
      this.scrollToBottom()
    }, 300)
  },

  /**
   * 
   * @param {*} result 接口返回的结果
   * @param {*} sourceId 答案对应用户提出的问题的id
   */
  getData(result, sourceId) {
    const data = result.data

    if (data.sessionId) {
      sessionId = data.sessionId
    }

    result.sourceId = sourceId

    const inputList = this.data.inputList.concat(customTypeToInputList(result))

    inputList.forEach((item, index) => {
      if (item.me && item.id === sourceId) {
        inputList[index] = Object.assign(inputList[index], { voiceText: result.question })
      }
    })

    this.setData({
      inputList
    })

    setTimeout(() => {
      this.scrollToBottom()
    }, 300)
  },

  fetchData(opt = {}) {
    if (opt.type === 'voice') {

      upload({
        // url: `${util.host}/aide/saas/chat/voice`,
        url: `${util.host}/aide/saas/chat/v1/voice`,
        withLocation: true,
        filePath: opt.tempFilePath,
        name: 'upload_file',
        formData: sessionId ? {
          sessionId: sessionId
        } : {}
      }).then(res => {
        wx.hideLoading()
        let result = res.data
        result = JSON.parse(result)

        if (result.status === 0 && result.data) {
          //正常情况
          this.getData(result, opt.myInputId)
        } else {
          //语音问题为空的情况
          this.fetchErrFunc()
        }

        setTimeout(() => {
          this.scrollToBottom()
        }, 300)

      }).catch((err) => {
        //抛出异常的情况
        wx.hideLoading()
        this.fetchErrFunc()

        setTimeout(() => {
          this.scrollToBottom()
        }, 300)
      })
    } else {

      request({
        // url: `${util.host}/aide/saas/chat/word`,
        url: `${util.host}/aide/saas/chat/v1/word`,
        method: 'GET',
        withLocation: true,
        data: sessionId ? {
          question: this.data.inputWord,
          sessionId: sessionId
        } : {
            question: this.data.inputWord
          }
      }).then(res => {
        const result = res.data
        if (result.status === 0 && result.data) {
          this.getData(result, opt.myInputId)
        } else {
          this.fetchErrFunc()
        }
      }).catch(err => {
        throw Error('index288-error' + err)
      })
    }
  },
  scrollToBottom() {
    this.setData({
      toView: 'toView'
    })
  },
  changeToSoundMode() {
    eventTracking('click', 'index_change_to_sound_btn')
    this.setData({
      mode: 'sound'
    })
    setTimeout(() => {
      this.scrollToBottom()
    }, 300)
  },
  changeToTextMode() {
    eventTracking('click', 'index_change_to_text_btn')
    this.setData({
      mode: 'text'
    })
  },
  soundStart(e) {
    speakDuration = Date.now()
    if (!recording) {
      wx.getSetting({
        success: (res) => {
          if (!res.authSetting['scope.record']) {
            wx.authorize({
              scope: 'scope.record',
              fail: function () {
                store.data.hasAuthRecord = false
                store.update()
              }
            })
          }
        }
      })
      recording = true
      this.recorderManager.start({
        format: 'mp3',
        sampleRate: 44100,
        encodeBitRate: 160000
      })
    }
  },
  soundFinish() {
    if (recording) {
      setTimeout(() => {
        speakDuration = Math.round((Date.now() - speakDuration) / 1000)
        this.recorderManager.stop()
      }, 500)

    }
  },
  cancel() {
    this.soundFinish()
  },

  menuToggle() {
    eventTracking('click', 'index_menu_btn')
    if (!this.data.showMenu) {
      setTimeout(() => {
        this.scrollToBottom()
      }, 100)
    }
    this.setData({
      showMenu: !this.data.showMenu
    })
  },
  tapScrollView() {
    if (this.data.showMenu) {
      this.setData({
        showMenu: false
      })
    }
  },
  test() {
    wx.navigateToMiniProgram({
      appId: 'wxd804d46617a8e29e',
      path: 'pagesA/myPages/complain/complain'
    })
  }
})

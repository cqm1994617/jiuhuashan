import create from '../../utils/create'
import store from '../../store/index'
import QQMapWX from '../../lib/qqmap-wx-jssdk.min.js'
import amapFile from '../../lib/amap-wx.js'
import requestObj from '../../utils/request'
import util from '../../utils/util'

const request = requestObj.request
let qqmapsdk = null
let mapView = null
let myAmapFun = null

function amapDirection(opt = {}) {
  return new Promise((resolve) => {
    myAmapFun.getWalkingRoute({
      origin: opt.origin,
      destination: opt.destination,
      success(data) {
        let points = [];
        if (data.paths && data.paths[0] && data.paths[0].steps) {
          let steps = data.paths[0].steps;
          for (let i = 0; i < steps.length; i++) {
            let poLen = steps[i].polyline.split(';');
            for (let j = 0; j < poLen.length; j++) {
              points.push({
                longitude: parseFloat(poLen[j].split(',')[0]),
                latitude: parseFloat(poLen[j].split(',')[1])
              })
            }
          }
        }
        resolve({
          points: points,
          distance: Number(data.paths[0].distance),
          duration: Number(data.paths[0].duration)
        })
      }
    })
  })
}

let lineId = ''
let lineName = ''

create(store, {
  data: {
    showModal: false,
    routeList: [],
    tourRoute: {},
    distance: '',
    markers: [],
    tourRoutePage: {},
    activeItem: {},
    latitude: '30.00',
    longitude: '122.39',
    windowHeight: 800,
    scale: 13,
    audio: null,
    backgroundAudioId: null,
    backgroundAudioPause: false
  },

  onShareAppMessage() {
    return {
      title: lineName ? lineName : '路线规划',
      path: '/pages/tourRoute/index?id=' + lineId
    }
  },

  onLoad(option) {
    this.backgroundAudio = wx.getBackgroundAudioManager()

    this.backgroundAudio.onStop(() => {
      this.store.data.backgroundAudioPause = false
      this.store.data.backgroundAudioId = null
      this.update()
    })
    this.backgroundAudio.onEnded(() => {
      this.store.data.backgroundAudioPause = false
      this.store.data.backgroundAudioId = null
      this.update()
    })

    const systemInfo = wx.getSystemInfoSync()

    myAmapFun = new amapFile.AMapWX({ key: 'a1c1ae4f0743dac7964083a09f82116d' })

    // const route = this.store.data.tourRoutePage.tourline.tourlineDetailInfoList
    const tourlineStorage = this.store.data.tourRoutePage.tourlineStorage

    request({
      url: `${util.host}/aide/saas/data/line?id=${option.id}`
    }).then(res => {

      const data = res.data.data

      lineId = data.id
      lineName = data.lineName

      const route = data.tourlineDetailInfoList

      let arr = []
      this.setData({
        routeList: route,
        tourRoutePage: data,
        windowHeight: systemInfo.windowHeight,
        markers: route.map(item => ({
          id: item.sort,
          latitude: item.latitude,
          longitude: item.longitude,
          width: 24,
          height: 24,
          iconPath: `../../img/circle/circle_${item.sort < 10 ? '0' + item.sort : item.sort}.png`
        }))
      })

      for (let i = 0; i < route.length; i++) {
        if (route[i + 1]) {
          arr.push({
            origin: `${route[i].longitude},${route[i].latitude}`,
            destination: `${route[i + 1].longitude},${route[i + 1].latitude}`
          })
        }
      }

      let haveStorage = false, tourStorageItem = null

      for (let i = 0; i < tourlineStorage.length; i++) {
        if (tourlineStorage[i].id === data.id) {
          if (tourlineStorage[i].hashValue === data.hashValue) {
            haveStorage = true
            tourStorageItem = tourlineStorage[i]
          } else {
            //hash不同则缓存过期，删除指定项
            this.store.data.tourRoutePage.tourlineStorage.splice(i, 1)
            this.update()
          }
        }
      }

      if (haveStorage) {
        this.setData({
          distance: tourStorageItem.distance,
          polyline: tourStorageItem.polyline
        })
      } else {
        Promise.all(
          arr.map(item => amapDirection(item))
        ).then(res => {
          const distance = res.map(item => item.distance).reduce((a, b) => a + b)

          this.store.data.tourRoutePage.tourlineStorage.push({
            id: data.id,
            hashValue: data.hashValue,
            distance: distance > 1000 ? (distance / 1000).toFixed(1) + 'km' : distance + 'm',
            polyline: res.map(item => ({
              points: item.points,
              color: '#FF8C00',
              width: 3
            }))
          })
          this.update()

          this.setData({
            distance: distance > 1000 ? (distance / 1000).toFixed(1) + 'km' : distance + 'm',
            polyline: res.map(item => ({
              points: item.points,
              color: '#FF8C00',
              width: 3
            }))
          })
        })
      }
    })

  },
  getScenicDetail(id) {
    return request({
      url: `${util.host}/aide/saas/data/scenic?id=${id}`
    })
  },
  getAudioDetail(id) {
    return request({
      url: `${util.host}/aide/saas/data/scenic/getVoiceById?scenicId=${id}`
    })
  },
  marketTap(e) {
    const selectItem = this.data.routeList.filter(item => e.markerId === item.sort)[0]

    if (selectItem.sort !== this.data.activeItem.sort) {

      this.setData({
        audio: null
      })

      this.getScenicDetail(selectItem.scenicId).then(res => {
        selectItem.desc = res.data.data.description
        this.setData({
          activeItem: selectItem,
          latitude: selectItem.latitude,
          longitude: selectItem.longitude,
          scale: 16,
          showModal: true
        })
        return this.getAudioDetail(res.data.data.id)
      }).then(res => {
        this.setData({
          audio: res.data.data || null
        })
      })
    } else {
      this.setData({
        showModal: true
      })
    }
  },
  selectScenic(e) {
    const scenicInfo = e.currentTarget.dataset.scenicinfo

    if (scenicInfo.sort !== this.data.activeItem.sort) {

      this.setData({
        audio: null
      })

      this.getScenicDetail(scenicInfo.scenicId).then(res => {
        scenicInfo.desc = res.data.data.description
        this.setData({
          activeItem: scenicInfo,
          latitude: scenicInfo.latitude,
          longitude: scenicInfo.longitude,
          scale: 16,
          showModal: true
        })
        return this.getAudioDetail(res.data.data.id)
      }).then(res => {
        this.setData({
          audio: res.data.data || null
        })
      })
    } else {
      this.setData({
        showModal: true
      })
    }
  },
  closeModal() {
    this.setData({
      showModal: false
    })
  },
  audioPlay(e) {
    if ((this.store.data.backgroundAudioId === e.currentTarget.dataset.voiceid) && (!this.store.data.backgroundAudioPause)) {
      //暂停
      this.store.data.backgroundAudioPause = !this.backgroundAudio.paused
      this.backgroundAudio.pause()
      this.update()
    } else {
      if (this.backgroundAudio.paused && (this.store.data.backgroundAudioId === e.currentTarget.dataset.voiceid)) {
        this.backgroundAudio.play()
        this.store.data.backgroundAudioPause = false
      } else {
        this.backgroundAudio.src = e.currentTarget.dataset.voiceurl
        this.backgroundAudio.title = e.currentTarget.dataset.title || ''
        this.store.data.backgroundAudioId = e.currentTarget.dataset.voiceid || ''
        this.store.data.backgroundAudioPause = false
        this.backgroundAudio.play()
      }
      this.update()
    }
  },
  scenicDetail(e) {
    wx.navigateTo({
      url: `../scenicIntroDetail/index?id=${e.currentTarget.dataset.scenicid}`
    })
  }
})
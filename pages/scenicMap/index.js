import create from '../../utils/create'
import store from '../../store/index'
import QQMapWX from '../../lib/qqmap-wx-jssdk.min.js'
import scenicList from './scenicData'

let qqmapsdk = null
let mapView = null

create(store, {
  data: {
    scenicList: scenicList.filter(item => item.scale < 11).map((item) => ({
      title: item.scenicName,
      latitude: item.lat,
      longitude: item.lng,
      id: item.id,
      scale: item.scale,
      scenicId: item.scenicId,
      iconPath: '/img/mapLocation.png',
      width: 30,
      height: 30,
      label: {
        content: item.scenicName,
        textAlign: 'center',
        bgColor: '#ff4500',
        borderRadius: 3,
        fontSize: 10,
        padding: 2,
        x: item.scenicName.length * (-10) / 2 - 3,
        anchorX: item.scenicName.length * (-10) / 2 - 3,
        color: '#fff'
      }
    })),
    polyline: []
  },
  onShareAppMessage() {
    return {
      title: '景区地图'
    }
  },
  onLoad() {
    mapView = wx.createMapContext('mapView')
    // qqmapsdk = new QQMapWX({
    //   key: 'FWHBZ-KRB6U-A7LVH-B2BVL-UYHM2-4UBHQ'
    // })
    // qqmapsdk.direction({
    //   mode: 'walking',//可选值：'driving'（驾车）、'walking'（步行）、'bicycling'（骑行），不填默认：'driving',可不填
    //   //from参数不填默认当前地址
    //   from: {
    //     latitude: 29.977285,
    //     longitude: 122.393863
    //   },
    //   to: {
    //     latitude: 30.000653,
    //     longitude: 122.395389
    //   },
    //   success: (res) => {
    //     let coors = res.result.routes[0].polyline, pl = []
    //     for (var i = 2; i < coors.length; i++) {
    //       coors[i] = coors[i - 2] + coors[i] / 1000000
    //     }
    //     for (var i = 0; i < coors.length; i += 2) {
    //       pl.push({ latitude: coors[i], longitude: coors[i + 1] })
    //     }
    //     console.log(pl)
    //     console.log(res)
    //     this.setData({
    //       polyline: [{
    //         points: pl,
    //         color: '#FF00DD',
    //         width: 4
    //       }]
    //     })
    //   }
    // })
  },
  regionchange() {
    if (mapView) {
      mapView.getScale({
        success: (res) => {
          const list = scenicList.filter(item => item.scale < res.scale).map((item) => ({
            title: item.scenicName,
            latitude: item.lat,
            longitude: item.lng,
            id: item.id,
            scale: item.scale,
            scenicId: item.scenicId,
            
            label: {
              content: item.scenicName,
              textAlign: 'center',
              bgColor: '#ff4500',
              borderRadius: 3,
              fontSize: 12,
              padding: 3,
              x: item.scenicName.length * (-12) / 2 - 3,
              anchorX: item.scenicName.length * (-12) / 2 - 3,
              color: '#fff'
            },
            iconPath: '/img/mapLocation.png',
            width: 30,
            height: 30
          }))
          this.setData({
            scenicList: list
          })
        }
      })
    }
  },
  openLocation(e) {
    const scenic = scenicList.filter(item => item.id === e.markerId)[0]
    wx.openLocation({
      latitude: scenic.lat,
      longitude: scenic.lng,
      name: scenic.scenicName
    })
  }
})

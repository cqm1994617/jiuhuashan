const data = [
  {
    id: 0,
    scenicName: '花台景区',
    scenicId: 441,
    scale: 3,
    lat: 30.501089,
    lng: 117.811072
  },
  {
    id: 1,
    scenicName: '莲花峰',
    scenicId: 488,
    scale: 3,
    lat: 30.566804,
    lng: 117.837888
  },
  {
    id: 2,
    scenicName: '甘露寺',
    scenicId: 456,
    scale: 3,
    lat: 30.493969,
    lng: 117.801758
  },
  {
    id: 3,
    scenicName: '慧居寺',
    scenicId: 457,
    scale: 3,
    lat: 30.472176,
    lng: 117.814537
  },
  {
    id: 4,
    scenicName: '狮子峰',
    scenicId: 505,
    scale: 3,
    lat: 30.462506,
    lng: 117.828225
  },
  {
    id: 5,
    scenicName: '万佛塔',
    scenicId: 466,
    scale: 3,
    lat: 30.473972,
    lng: 117.804621
  }
]

export default data

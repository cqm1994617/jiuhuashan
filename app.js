import util from './utils/util.js'
import store from './store/index'
const wxLogin = util.promisify(wx.login)
const wxRequest = util.promisify(wx.request)

function init(token, opt) {
  wx.getSystemInfo({
    success: (res) => {
      wxRequest({
        url: `${util.host}/aide/saas/equipment/insert`,
        method: 'POST',
        data: {
          token: token,
          model: res.model || '',
          version: res.version || '',
          system: res.system || '',
          scene: opt.scene ? opt.scene.toString() : '',
          qrSource: opt.query.qrSource || ''
        },
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'X-Authorization': token,
          'X-Source': util.xSource,
        }
      })
    }
  })
}

function wxUpdate() {

  if (wx.canIUse('getUpdateManager')) {
    const updateManager = wx.getUpdateManager()

    updateManager.onUpdateReady(function () {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success(res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })

    updateManager.onUpdateFailed(function() {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经上线啦~，您可以删除当前小程序，重新搜索打开哟~',
        showCancel: false
      })
    })
  }
}

//app.js
App({
  onLaunch: function (opt) {
    console.log('launch')
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    const token = wx.getStorageSync('token')
    // if (!token) {

    wxLogin().then((codeInfo) => {
      wxRequest({
        url: `${util.host}/aide/client/wechat/code2Session?code=${codeInfo.code}`,
        header: {
          tenant: 'JIUHUASHAN'
        }
      }).then(res => {
        if (res.data && res.data.data) {
          wx.setStorageSync('token', res.data.data)
          init(res.data.data, opt)
        }
      })
    })
    // } else {
    // init(token, opt)
    // }

    const isFirst = wx.getStorageSync('isFirst')
    if (!isFirst) {
      wx.setStorageSync('isFirst', '1')
    }

    setTimeout(() => {
      store.data.isFullScreen = util.isFullScreen()
      store.update()
    }, 200)

    wx.getSetting({
      success: (res) => {
        if (!res.authSetting['scope.userLocation']) {
          wx.authorize({
            scope: 'scope.userLocation',
            fail: function () {
              store.data.hasAuthLocation = false
              store.update()
            }
          })
        }
        if (!res.authSetting['scope.record']) {
          wx.authorize({
            scope: 'scope.record',
            fail: function () {
              store.data.hasAuthRecord = false
              store.update()
            }
          })
        }
      }
    })
    
    wxUpdate()

    // wx.getSetting({
    //   success: res => {
    //     if (res.authSetting['scope.userInfo']) {
    //       // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
    //       wx.getUserInfo({
    //         success: res => {
    //           // 可以将 res 发送给后台解码出 unionId

    //           store.data.userInfo = res
    //           store.data.index.hasUserInfo = true
    //           this.globalData.userInfo = res

    //           store.update()
    //         }
    //       })
    //     } else {
    //       // store.data.index.showAuthModal = true
    //       // store.update()
    //     }
    //   }
    // })

    // wx.getSetting({
    //   success: res => {
    //     if (res.authSetting['scope.userInfo']) {
    //       // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
    //       wx.getUserInfo({
    //         success: res => {
    //           // 可以将 res 发送给后台解码出 unionId
    //           this.globalData.userInfo = res

    //           // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
    //           // 所以此处加入 callback 以防止这种情况
    //           if (this.userInfoReadyCallback) {
    //             this.userInfoReadyCallback(res)
    //           }
    //         }
    //       })
    //     }
    //   }
    // })

  },

  globalData: {
    userInfo: null
  }
})

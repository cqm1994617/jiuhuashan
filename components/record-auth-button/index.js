import create from '../../utils/create'

create({
  data: {
    canIuse: true
  },

  ready() {

    this.setData({
      canIUse: wx.canIUse("button.open-type.openSetting")
    })
  },

  methods: {

    oldVersionOpenSetting() {
      const _this = this

      wx.getSetting({
        success: (res) => {
          if (!res.authSetting['scope.record']){
            wx.openSetting({
              success: function() {
                _this.store.data.hasAuthRecord = true
                _this.update()
              }
            })
          }
        }
      })
    },
    openSetting(e) {
      if (e.detail.authSetting['scope.record']) {
        this.store.data.hasAuthRecord = true
        this.update()
      }
    }
  }

})

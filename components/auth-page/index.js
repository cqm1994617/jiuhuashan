import create from '../../utils/create'
import util from '../../utils/util'

const wxRequest = util.promisify(wx.request)

create({

  data: {
    canIUse: true
  },

  ready() {

    this.setData({
      canIUse: wx.canIUse("button.open-type.openSetting")
    })
  },

  methods: {

    oldVersionOpenSetting() {

      const _this = this

      wx.getSetting({
        success: (res) => {
          if (!res.authSetting['scope.userLocation']) {
            wx.openSetting({
              success: function (res) {
                if (res.authSetting['scope.userLocation']) {
                  _this.store.data.hasAuthLocation = true
                  _this.update()
                }
                if (res.authSetting['scope.record']) {
                  _this.store.data.hasAuthRecord = true
                  _this.update()
                }
              }
            })
          }
        }
      })
    },
    openSetting(e) {

      if (e.detail.authSetting['scope.userLocation']) {
        this.store.data.hasAuthLocation = true
        this.update()
      }
      if (e.detail.authSetting['scope.record']) {
        this.store.data.hasAuthRecord = true
        this.update()
      }
    }
  }
})

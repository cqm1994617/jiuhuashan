Component({
  properties: {
    duration: Number,
    url: String
  },

  data: {
    start: false,
    currentTime: 0,
    isScrolling: false,
    durationFormat: '',
    currentTimeFormat: '00:00'
  },

  ready() {
    this.setData({
      durationFormat: this.timeFormat(this.data.duration)
    })

    this.innerAudioContext = wx.createInnerAudioContext()
    this.innerAudioContext.src = this.data.url
    this.innerAudioContext.onPlay(() => {
    })
    this.innerAudioContext.onTimeUpdate(() => {
      if (!this.data.isScrolling) {
        this.setData({
          currentTime: Math.floor(this.innerAudioContext.currentTime),
          currentTimeFormat: this.timeFormat(Math.floor(this.innerAudioContext.currentTime))
        })
      }
    })
    this.innerAudioContext.onStop(() => {
      this.setData({
        start: false
      })
    })
    this.innerAudioContext.onEnded(() => {
      this.setData({
        start: false
      })
    })

  },

  methods: {
    audioToggle() {
      if (this.data.start) {
        this.innerAudioContext.pause()
      } else {
        this.innerAudioContext.play()
      }
      this.setData({
        start: !this.data.start
      })
    },
    scollingFinish(res) {
      this.innerAudioContext.seek(res.detail.value)
      this.setData({
        currentTime: res.detail.value,
        currentTimeFormat: this.timeFormat(res.detail.value),
        isScrolling: false
      })
    },
    scrolling() {
      if (!this.data.isScrolling) {
        this.setData({
          isScrolling: true
        })
      }
    },
    timeFormat(time) {
      return this.formatNumber(Math.floor(time / 60)) + ':' + this.formatNumber(time % 60)
    },
    formatNumber(num) {
      return num >= 10 ? num : '0' + num
    }
  }
})

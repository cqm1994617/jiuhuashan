const app = getApp()
import create from '../../utils/create'

create({
  data: {

  },
  methods: {
    getUserInfo(e) {
      app.globalData.userInfo = e.detail
      this.store.data.userInfo = e.detail
      this.store.data.index.hasUserInfo = true
      this.store.data.index.showAuthModal = false
      this.update()
    }
  }
})

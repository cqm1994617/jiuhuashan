const app = getApp()
import create from '../../utils/create'
import util from '../../utils/util'
import eventTracking from '../../utils/eventTracking'

create({
  properties: {
    linkStr: String
  },
  data: {
    text: '',
    appId: '',
    appPath: ''
  },
  ready() {
    const str = this.data.linkStr
    const text = str.slice(str.indexOf('[') + 1, str.indexOf(']'))
    const appInfo = str.slice(str.indexOf('(') + 1, str.indexOf(')'))
    const appId = appInfo.split('|')[0]
    const appPath = appInfo.split('|')[1]

    this.setData({
      appId,
      appPath,
      text
    })
  },
  methods: {
    wxappLink(e) {
      eventTracking('click', 'wxapp_link', this.data.text)

      wx.navigateToMiniProgram({
        appId: this.data.appId,
        path: this.data.appPath
      })
    }
  }
})

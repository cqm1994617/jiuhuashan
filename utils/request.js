import util from './util'
import store from '../store/index'

const wxRequest = util.promisify(wx.request)
const wxUploadFile = util.promisify(wx.uploadFile)
const wxGetLocation = util.promisify(wx.getLocation)
const wxLogin = util.promisify(wx.login)

const request = (obj = {}) => {

  let startPromise = null

  const token = getApp().globalData.token

  if (token) {
    startPromise = Promise.resolve()
  } else {
    startPromise = wxLogin().then(codeInfo => {
      return wxRequest({
        url: `${util.host}/aide/client/wechat/code2Session?code=${codeInfo.code}`,
        header: {
          'tenant': 'JIUHUASHAN'
        }
      }).then(res => {
        if (res.data && res.data.data) {
          getApp().globalData.token = res.data.data
        }
      })
    })
  }

  if (obj.withLocation) {
    return wxGetLocation({
      type: 'gcj02'
    }).then(res => {
      store.data.mineLocation = {
        latitude: res.latitude,
        longitude: res.longitude
      }
      store.update()
      return startPromise.then(() => {
        return wxRequest({
          ...obj,
          url: obj.url,
          data: obj.data ? {
            ...obj.data,
            // sessionId: '',  //暂时不加
            lat: res ? res.latitude : '',
            lng: res ? res.longitude : ''
          } : {
              lat: res ? res.latitude : '',
              lng: res ? res.longitude : ''
            },
          header: {
            'X-Authorization': getApp().globalData.token,
            'X-Source': util.xSource,
            'tenant': 'JIUHUASHAN',
            ...obj.header
          }
        })
      })
    }).catch(() => {
      // 经纬度为空，默认定位杭州
      wx.getSetting({
        success: (res) => {
          if (!res.authSetting['scope.userLocation']) {
            wx.authorize({
              scope: 'scope.userLocation',
              fail: function () {
                store.data.hasAuthLocation = false
                store.update()
              }
            })
          }
        }
      })
      return startPromise.then(() => {
        return wxRequest({
          ...obj,
          url: obj.url,
          data: obj.data ? {
            ...obj.data,
            // sessionId: '',  //暂时不加
            lat: store.data.mineLocation.latitude || '30.280051',
            lng: store.data.mineLocation.longitude || '120.01615'
          } : {
            lat: store.data.mineLocation.latitude || '30.280051',
            lng: store.data.mineLocation.longitude ||'120.01615'
          },
          header: {
            'X-Authorization': getApp().globalData.token,
            'X-Source': util.xSource,
            'tenant': 'JIUHUASHAN',
            ...obj.header
          }
        })
      })

    })
  }

  return startPromise.then(() => {
    return wxRequest({
      ...obj,
      url: obj.url,
      data: obj.data ? {
        ...obj.data,
      } : {},
      header: {
        'X-Authorization': getApp().globalData.token,
        'X-Source': util.xSource,
        'tenant': 'JIUHUASHAN',
        ...obj.header
      }
    })
  })
}

const upload = (obj = {}) => {

  let startPromise = null
  const token = getApp().globalData.token

  if (token) {
    startPromise = Promise.resolve()
  } else {
    startPromise = wxLogin().then(codeInfo => {
      return wxRequest({
        url: `${util.host}/aide/client/wechat/code2Session?code=${codeInfo.code}`
      }).then(res => {
        if (res.data && res.data.data) {
          getApp().globalData.token = res.data.data
        }
      })
    })
  }

  if (obj.withLocation) {
    return wxGetLocation({
      type: 'gcj02'
    }).then(res => {
      return startPromise.then(() => {
        return wxUploadFile({
          ...obj,
          header: {
            'X-Authorization': getApp().globalData.token,
            'X-Source': util.xSource,
            'tenant': 'JIUHUASHAN',
            ...obj.header
          },
          formData: obj.formData ? {
            ...obj.formData,
            lat: res ? res.latitude : '',
            lng: res ? res.longitude : ''
          } : {
              lat: res ? res.latitude : '',
              lng: res ? res.longitude : ''
            }
        })
      })
    }).catch(err => {
      wx.getSetting({
        success: (res) => {
          if (!res.authSetting['scope.userLocation']) {
            wx.authorize({
              scope: 'scope.userLocation',
              fail: function () {
                store.data.hasAuthLocation = false
                store.update()
              }
            })
          }
        }
      })
      return startPromise.then(() => {
        return wxUploadFile({
          ...obj,
          header: {
            'X-Authorization': getApp().globalData.token,
            'X-Source': util.xSource,
            'tenant': 'JIUHUASHAN',
            ...obj.header
          },
          formData: obj.formData ? {
            ...obj.formData,
            lat: store.data.mineLocation.latitude || '30.280051',
            lng: store.data.mineLocation.longitude || '120.01615'
          } : {
            lat: store.data.mineLocation.latitude || '30.280051',
            lng: store.data.mineLocation.longitude || '120.01615'
          }
        })
      })
    })
  }

  return startPromise.then(() => {
    return wxUploadFile({
      ...obj,
      header: {
        'X-Authorization': getApp().globalData.token,
        'X-Source': util.xSource,
        'tenant': 'JIUHUASHAN',
        ...obj.header
      },
      formData: obj.formData ? {
        ...obj.formData,
      } : {}
    })
  })

}

export {
  request,
  upload
}

export default {
  request,
  upload
}

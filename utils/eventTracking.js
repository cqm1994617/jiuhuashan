import util from './util'
import requestObj from './request'

const request = requestObj.request

function eventTracking(type, name, data) {
  request({
    url: `${util.host}/aide/saas/event/insert`,
    method: 'POST',
    data: {
      data: {
        eventType: type,
        eventName: name,
        data: data || ''
      }
    }
  })
}

export default eventTracking


const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const isFullScreen = () => {
  const res = wx.getSystemInfoSync()
  return (/iPhone X/).test(res.model)
}


const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const promisify = (fn) => {
  return (obj = {}) => {
    return new Promise((resolve, reject) => {
      obj.success = (res) => {
        resolve(res)
      }
      obj.fail = (err) => {
        reject(err)
      }

      fn(obj)
    })
  }
}

const flat = (arr) => {
  let res = []
  function merge(childArr) {
    for (let i = 0; i < childArr.length; i++) {
      if (Object.prototype.toString.call(childArr[i]) === '[object Array]') {
        merge(childArr[i])
      } else {
        res.push(childArr[i])
      }
    }
  }
  merge(arr)

  return res
}

const htmlTextToRichText = (text) => {
  return text.replace(/<(?:.|\n)*?>/gm, '')
    .replace(/<\?xml.*\?>\n/, '')
    .replace(/<\?xml.*\?>\n/, '')
    .replace(/<.*!doctype.*\>\n/, '')
    .replace(/<.*!DOCTYPE.*\>\n/, '').replace(/\r?\n+/g, '')
    .replace(/<!--.*?-->/ig, '')
    .replace(/\/\*.*?\*\//ig, '')
    .replace(/[ ]+</ig, '<')
    .replace(/&nbsp;/ig, '')
    .replace(/&gt;/g, '>')
    .replace(/&lt;/g, '<')
    .replace(/&amp;/g, '&')
    .replace(/&quot;/g, '\"')
}

const richTextToArray = (richText) => {

  const paragraph = richText.split(/\#br\#|\$br\$/)

  function deal(item) {

    let temp = item.split(/\#image\#|\$image\$/).map((data, index) => {
      return index % 2 === 0 ? data : {
        index: `img${index}`,
        type: 'img',
        data: data ? decodeURIComponent(data) : ''
      }
    })

    temp.forEach((data, index) => {
      if (typeof data === 'string' && (data.indexOf('$link$') > -1 || data.indexOf('#link#') > -1)) {
        temp[index] = data.split(/\#link\#|\$link\$/).map((cItem, cIndex) => {
          return cIndex % 2 === 0 ? cItem : {
            index: `link${cIndex}`,
            type: 'link',
            data: cItem
          }
        })
      }
    })

    temp = flat(temp)

    temp.forEach((data, index) => {
      if (typeof data === 'string' && data.indexOf('$h1$') > -1) {
        temp[index] = data.split('$h1$').map((cItem, cIndex) => {
          return cIndex % 2 === 0 ? cItem : {
            index: `h1_${cIndex}`,
            type: 'h1',
            data: cItem
          }
        })
      }
    })

    temp = flat(temp)

    temp.forEach((data, index) => {
      if (typeof data === 'string' && data.indexOf('$h2$') > -1) {
        temp[index] = data.split('$h2$').map((cItem, cIndex) => {
          return cIndex % 2 === 0 ? cItem : {
            index: `h2_${cIndex}`,
            type: 'h2',
            data: cItem
          }
        })
      }
    })

    temp = flat(temp)

    temp.forEach((data, index) => {
      if (typeof data === 'string' && data.indexOf('$h3$') > -1) {
        temp[index] = data.split('$h3$').map((cItem, cIndex) => {
          return cIndex % 2 === 0 ? cItem : {
            index: `h3_${cIndex}`,
            type: 'h3',
            data: cItem
          }
        })
      }
    })

    temp = flat(temp)

    temp.forEach((data, index) => {
      if (typeof data === 'string' && (data.indexOf('$phone$') > -1 || data.indexOf('#phone#') > -1)) {
        temp[index] = data.split(/\#phone\#|\$phone\$/).map((cItem, cIndex) => {
          return cIndex % 2 === 0 ? cItem : {
            index: `phone${cIndex}`,
            type: 'phone',
            data: cItem
          }
        })
      }
    })

    temp = flat(temp)

    temp.forEach((data, index) => {
      if (typeof data === 'string' && data.indexOf('$strong$') > -1) {
        temp[index] = data.split('$strong$').map((cItem, cIndex) => {
          return cIndex % 2 === 0 ? cItem : {
            index: `strong${cIndex}`,
            type: 'strong',
            data: cItem
          }
        })
      }
    })

    temp = flat(temp)

    temp.forEach((data, index) => {
      if (typeof data === 'string' && data.indexOf('$wxapp_link$') > -1) {
        temp[index] = data.split('$wxapp_link$').map((cItem, cIndex) => {
          return cIndex % 2 === 0 ? cItem : {
            index: `wxapp_link${cIndex}`,
            type: 'wxapp_link',
            data: cItem
          }
        })
      }
    })

    temp = flat(temp)

    temp.forEach((data, index) => {
      if (typeof data === 'string' && data.indexOf('$web_url$') > -1) {
        temp[index] = data.split('$web_url$').map((cItem, cIndex) => {
          return cIndex % 2 === 0 ? cItem : {
            index: `web_url${cIndex}`,
            type: 'web_url',
            data: cItem
          }
        })
      }
    })

    temp = flat(temp)

    temp.forEach((data, index) => {
      if (typeof data === 'string') {
        temp[index] = {
          index: `text${index}`,
          type: 'text',
          data: temp[index]
        }
      }
    })

    return temp.filter(_ => _)

  }

  const res = []

  paragraph.forEach(item => {
    res.push(deal(item))
  })

  return res

}

const getSummary = (richArr) => {

  let length = 0, maxLength = 150, res = [], hasMore = false

  for (let i = 0; i < richArr.length; i++) {

    res[i] = []

    for (let j = 0; j < richArr[i].length; j++) {
      if (length >= maxLength) {
        hasMore = true
        return {
          hasMore: hasMore,
          summary: res
        }
      }

      if (richArr[i][j].type === 'text') {

        if (length + richArr[i][j].data.length >= maxLength) {
          res[i].push({
            type: 'text',
            index: richArr[i][j].index,
            data: richArr[i][j].data.slice(0, maxLength - length) + (length + richArr[i][j].data.length >= maxLength ? '......' : '')
          })
          hasMore = true

        } else {
          res[i].push(richArr[i][j])
        }
        length += richArr[i][j].data.length

      } else if (richArr[i][j].type === 'img') {
        if (length + 100 >= maxLength) {
          res[i].push(richArr[i][j])
          hasMore = true
        } else {
          res[i].push(richArr[i][j])
        }
        length += richArr[i][j].data.length
      } else {
        res[i].push(richArr[i][j])
      }

    }
  }

  return {
    hasMore: hasMore,
    summary: res
  }

}

//小于10的数字补0
function digitalCompletion(num) {
  if (typeof num !== 'number') {
    throw new Error('digitalCompletion方法参数类型错误')
  }

  return num < 10 ? '0' + num : num
}

export default {
  htmlTextToRichText: htmlTextToRichText,
  formatTime: formatTime,
  isFullScreen: isFullScreen,
  promisify: promisify,
  richTextToArray: richTextToArray,
  getSummary: getSummary,
  digitalCompletion: digitalCompletion,
  xSource: 'wxapp-02',
  // host: 'http://192.168.3.191:12310'
  // host: 'http://47.110.151.199:12310'
  host: 'https://www.wenhualvyou.net/platform'
}
